package com.example.controller.drive;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class DriveController {
    @RequestMapping("/")
    public String index1(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //response.sendRedirect("/files");
        return "redirect:/files";
    }

    @GetMapping("/files")
    public String files(HttpServletRequest request, HttpServletResponse response) throws IOException {
        return "index";
    }
}
